<?php
return array(
	//'配置项'=>'配置值'
	'SHOW_PAGE_TRACE'	=> true,
	'TMPL_CACHE_ON'		=> false,
	'TMPL_CACHE_TIME'	=> 1,
	'APP_AUTOLOAD_PATH' => 'Common.Model,Admin.Widget',
	
	//控制器
	'ctrs' => [
		'index' => '控制台',
		'user' 	=> '用户管理',
		'run' => '运营管理',
		'count'   => '数据统计',
		'setting' => '系统设置',
		'document'=> '接口调试',
	],
	//所有的动作
	'actions' => [
		'index' => [
			['adminList', '管理员列表'],
			['adminEdit', '管理员编辑'],
			['adminDel',  '管理员删除'],
			['roleList',  '角色管理'],
			['roleEdit',  '角色添加编辑'],
			['roleDel',   '角色删除'],
		],
		'user'=>[
			['index', 	 '用户管理'],
			['identify', '认证管理'],
		],
		'run' => [
			['task', '任务管理'],
			['meal', '套餐列表'],
			['order', '订单管理'],
			['message', '消息管理'],
		],
		'count' => [
			['index', '每日数据'],
		],
		'setting'=>[
			['index', '通用设置'],
		],
		'document' => [
			['index', '接口调试']
		]
	],
	//导航
	'nav'=>[
		'index' => [
			['index', 		'概况'],
			['adminList',	'管理员'],
			['roleList', 	'角色管理'],
			['phpInfo', 	'服务器信息'],
		],
		'user'=>[
			['index', '用户管理',[
				[ 'url'=>'/Admin/User/index', 	'name'=>'注册时间' ],
				[ 'url'=>'/Admin/User/loginTime', 	'name'=>'登陆时间' ],
				[ 'url'=>'/Admin/User/hei', 	'name'=>'小黑屋'],
			]],
			['phoList', '摄影师管理'],
		],
		'run' => [
			['index', '任务管理'],
			['meal', '套餐列表'],
			['order', '订单管理'],
			['message', '消息管理'],
			['feedback', '建议反馈'],
			['slide', '首页轮播图'],
		],
		'count' => [
			['index', '每日数据'],
			['order', '订单数据'],
			['user', '在线用户'],
		],
		'setting' => [
			['index', '通用设置'],
		],
		'document' => [
			['index', '接口调试'],
		],
	],
);