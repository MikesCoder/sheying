<?php 
/**
 * 微信app支付
 */
class WeixinAppPay{
	
	private $mch_id; //商户号
	private $appid; //应用id
	private $needCA; //是否需要证书
	private $notifyUrl;
	
	function __construct(){
		$this->mch_id 	= '1383918202';
		$this->appid 	= 'wx286515047507dfec';
		$this->key 		= 'shouji17764553811dianhua88693669';
		$this->notifyUrl= 'http://139.196.207.19/index/payNotify';
	}
	
	/**
	 * 下单 取支付参数
	 **/
	function getPayParam($order){
		//没有预付单
		if(! ($prepayid = $order['wx_prepay_id']) ){
			$wxOrder = $this->unifiedOrder($order);
			if(!( $prepayid = $wxOrder['prepay_id'] ))
				return false;
		}
		
		$d = [
			'appid' 		=> $this->appid,
			'partnerid' 	=> $this->mch_id,
			'prepayid' 		=> $prepayid,
			'package' 		=> 'Sign=WXPay',
			'noncestr' 		=> md5(time()),
			'timestamp'		=> time(),
		];
		$d['sign'] = $this->getSign($d);
		return $d;
	}
	//统一下单
	function unifiedOrder($order){
		$url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
		!$notifyUrl && $notifyUrl = $this->notifyUrl;
		$ip = $_SERVER['REMOTE_ADDR'];
		$d = [
			'spbill_create_ip' 	=> $ip,
			'body' 			=> $order['subject'], 			//商品描述
			'out_trade_no' 	=> $order['order_sn'],			//
			'total_fee' 	=> intval($order['total']*100),
			'notify_url' 	=> $notifyUrl,
			'trade_type' 	=> 'APP',
		];
		if(!($res = $this->exec($d, $url, true))) 
			return false;
		return $res;
	}
	
	//查询订单
	function orderQuery($order_sn){
		$url = 'https://api.mch.weixin.qq.com/pay/orderquery';
		$d = ['out_trade_no'=>$order_sn];
		if(!($res = $this->exec($d, $url, true))) 
			return false;
		return $res;
	}
	//关闭订单
	function orderClose($order_sn){
		$url = 'https://api.mch.weixin.qq.com/pay/closeorder';
		$d = ['out_trade_no'=>$order_sn];
		if(!($res = $this->exec($d, $url, true))) 
			return false;
	}
	//退款
	function orderRefund($order_sn, $total){
		$url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
		$total2 = intval($total*100);
		$d = [
			'out_trade_no'	=> $order_sn,
			'out_refund_no'	=> $order_sn . '_886',
			'total_fee' 	=> $total2,
			'refund_fee' 	=> $total2,
			'op_user_id' 	=> $this->mch_id,
		];
		
		if(!($res = $this->exec($d, $url, true))) 
			return false;
		if('SUCCESS' != $res['result_code']){
			return $this->setError($res['err_code_des']);
		}
		return true;
	}
	
	//支付结果通知
	function payNotify($callback){
		$str = file_get_contents('php://input');
		$d = self::xmlToArr($str);
		
		$arr = ['return_code'=> 'SUCCESS', 'return_msg'=>'OK'];
		if(!$d || !$this->checkSign($d, $d['sign'])){
			$arr = ['return_code'=> 'FAIL', 'return_msg'=>'签名错误'];
		}
		
		$d['total'] = $d['total_fee']/100;
		if(is_callable($callback) && !call_user_func($callback, $d)){
			$arr = ['return_code'=> 'FAIL', 'return_msg'=>'验证失败!'];
		}
		
		if('FAIL' == $arr['return_code']){
			\Think\log::write($arr['return_msg'].$str, 'err');
		}
		echo self::arrToXml($arr);exit;
	}
	
	//执行
	function exec($d, $url, $needCA = false){
		if(!$d || !is_array($d))
			return $this->setError('参数错误!');
		$base = [
			'appid'   => $this->appid,
			'mch_id'  => $this->mch_id,
			'nonce_str' => md5(time()),
		];
		$arr = array_merge($base, $d);
		$arr['sign'] = self::getSign($arr);
		$content = self::arrToXml($arr);
		$res = self::post($url, $content, $needCA);
		$arr = self::xmlToArr($res);
		
		if('SUCCESS' != $arr['return_code']){
			\Think\log::write($res, 'err');
			return $this->setError($arr['return_msg']);
		}
		return $arr;
	}
	
	//签名
	function getSign($d){
		ksort($d);
		foreach($d as $k=>$v){
			$str .= $k .'='. $v .'&';
		}
		$str .= 'key=' . $this->key;
		return strtoupper(md5($str));
	}
	
	//验签
	function checkSign($d, $sign){
		unset($d['sign']);
		if($sign == $this->getSign($d))
			return true;
		return false;
	}
	
	//xml 字符串转换为数组
	static function xmlToArr($str){
		$obj = simplexml_load_string($str,'SimpleXMLElement',LIBXML_NOCDATA);
		return json_decode(json_encode($obj, JSON_UNESCAPED_UNICODE), 1);
	}
	
	//数组转换成 xml
	static function arrToXml($arr){
		$str = '<xml>';
		foreach($arr as $k=>$v){
			$str .= '<' . $k . '>';
			$str .= $v;
			$str .= '</' . $k . '>';
		}
		$str .= '</xml>';
		return $str;
	}
	
	//post 数据
	static function post($url, $data = null, $needCA = false){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		if (!empty($data)){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		
		if($needCA){
			//默认格式为PEM，可以注释
			curl_setopt($curl,CURLOPT_SSLCERTTYPE,'PEM');
			curl_setopt($curl,CURLOPT_SSLCERT, __DIR__ . '/apiclient_cert.pem');
			//默认格式为PEM，可以注释
			curl_setopt($curl,CURLOPT_SSLKEYTYPE,'PEM');
			curl_setopt($curl,CURLOPT_SSLKEY, __DIR__ . '/apiclient_key.pem');
		}
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		curl_close($curl);
		return $output;
	}
	
	function setError($e){
		$this->error = $e;
		return false;
	}
	
	function getError(){
		return $this->error;
	}
}



